import logging
from mock import Mock
from peewee_migrate import Router

from app.models import dbp


def test_migrations(app_before_init_db):
    """The database migrations complete successfully."""
    app, conf_obj = app_before_init_db

    dbp.connect()

    if conf_obj.database.engine == "PostgresqlDatabase":
        dbp.execute_sql("DROP SCHEMA public CASCADE;")
        dbp.execute_sql("CREATE SCHEMA public;")
        dbp.execute_sql("GRANT ALL ON SCHEMA public TO public;")

    router = Router(dbp, migrate_dir="migrations", ignore=["basemodel"])
    router.run()

    router_modmail = Router(dbp, migrate_dir="modmail_migrations",
                            migrate_table="modmail_migrations_history",
                            ignore=["basemodel"])
    router_modmail.run()

    # Shut up a warning in rollback that we can't do anything about.
    logging.getLogger("peewee_migrate").warn = Mock()

    applied_modmail_migrations = list(router_modmail.done)
    applied_modmail_migrations.reverse()
    for m in applied_modmail_migrations:
        router_modmail.rollback(m)

    # peewee_migrate uses a class property where it should use an
    # object property, so the router we created above is now broken.
    # Make a new one.
    router = Router(dbp, migrate_dir="migrations", ignore=["basemodel"])
    applied_migrations = list(router.done)
    applied_migrations.reverse()

    # Make sure new rollbacks work.  The existing ones are what they are.
    for m in applied_migrations:
        if m == "029_message_read":
            break
        router.rollback(m)

    dbp.close()
