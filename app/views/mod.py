""" Mod endpoints """
from base64 import urlsafe_b64encode
import urllib.parse
from peewee import fn, JOIN
from flask import Blueprint, abort, url_for
from flask_babel import _
from flask_login import login_required, current_user
from ..models import (
    PostReportLog,
    CommentReportLog,
    User,
    Sub,
    SubMod,
    SubPost,
    SubPostReport,
    SubPostComment,
    SubPostCommentReport,
)
from ..misc import engine, getModSubs, getReports
from ..forms import BanUserSubForm, CreateReportNote
from .. import misc
import json


bp = Blueprint("mod", __name__)


@bp.route("/")
@login_required
def index():
    # Implemented by the modmail server.  This stub exists for use by url_for.
    abort(404)


@bp.route("/reports", defaults={"page": 1})
@bp.route("/reports/<int:page>")
@login_required
def reports(page):
    """ WIP: Open Report Queue """

    if not (
        SubMod.select().where(SubMod.user == current_user.uid) or current_user.can_admin
    ):
        abort(404)

    reports = getReports("mod", "open", page)

    return engine.get_template("mod/reports.html").render(
        {
            "reports": reports,
            "page": page,
            "sub": False,
            "subInfo": False,
            "subMods": False,
        }
    )


@bp.route("/reports/closed", defaults={"page": 1})
@bp.route("/reports/closed/<int:page>")
@login_required
def closed(page):
    """ WIP: Closed Reports List """

    if not (
        SubMod.select().where(SubMod.user == current_user.uid) or current_user.can_admin
    ):
        abort(404)

    reports = getReports("mod", "closed", page)

    return engine.get_template("mod/closed.html").render(
        {
            "reports": reports,
            "page": page,
            "sub": False,
            "subInfo": False,
            "subMods": False,
        }
    )


@bp.route("/reports/<sub>", defaults={"page": 1})
@bp.route("/reports/<sub>/<int:page>")
@login_required
def reports_sub(sub, page):
    """ WIP: Sub Report Queue """

    try:
        sub = Sub.get(fn.Lower(Sub.name) == sub.lower())
    except Sub.DoesNotExist:
        abort(404)

    subInfo = misc.getSubData(sub.sid)
    subMods = misc.getSubMods(sub.sid)

    if not (current_user.is_mod(sub.sid, 1) or current_user.is_admin()):
        abort(404)

    reports = getReports("mod", "open", page, sid=sub.sid)

    return engine.get_template("mod/sub_reports.html").render(
        {
            "sub": sub,
            "reports": reports,
            "page": page,
            "subInfo": subInfo,
            "subMods": subMods,
        }
    )


@bp.route("/reports/closed/<sub>", defaults={"page": 1})
@bp.route("/reports/closed/<sub>/<int:page>")
@login_required
def reports_sub_closed(sub, page):
    """ WIP: Sub Closed Reports """

    try:
        sub = Sub.get(fn.Lower(Sub.name) == sub.lower())
    except Sub.DoesNotExist:
        abort(404)

    if not (current_user.is_mod(sub.sid, 1) or current_user.is_admin()):
        abort(404)

    subInfo = misc.getSubData(sub.sid)
    subMods = misc.getSubMods(sub.sid)

    reports = getReports("mod", "closed", page, sid=sub.sid)

    return engine.get_template("mod/sub_reports_closed.html").render(
        {
            "sub": sub,
            "reports": reports,
            "page": page,
            "subInfo": subInfo,
            "subMods": subMods,
        }
    )


@bp.route("/reports/details/<sub>/<report_type>/<report_id>")
@login_required
def report_details(sub, report_type, report_id):
    """ WIP: Report Details View """

    try:
        sub = Sub.get(fn.Lower(Sub.name) == sub.lower())
    except Sub.DoesNotExist:
        return abort(404)

    if not (current_user.is_mod(sub.sid, 1) or current_user.is_admin()):
        return abort(404)

    subInfo = misc.getSubData(sub.sid)
    subMods = misc.getSubMods(sub.sid)

    report = getReports("mod", "all", 1, type=report_type, report_id=report_id)
    reported_user = User.select().where(User.name == report["reported"]).get()
    related_reports = getReports(
        "mod", "all", 1, type=report_type, report_id=report_id, related=True
    )
    related_report_ids = [rep["id"] for rep in related_reports["query"]]

    if report["type"] == "post":
        try:
            post = misc.getSinglePost(report["pid"])
            comment = ""
            logs = list(
                PostReportLog.select()
                .join(SubPostReport)
                .where(
                    (PostReportLog.rid == report["id"])
                    | (
                        (SubPostReport.pid == report["pid"])
                        & (
                            PostReportLog.action
                            == misc.LOG_TYPE_REPORT_MODMAIL_TO_REPORTED_USER
                        )
                    )
                )
                .order_by(PostReportLog.lid.desc())
            )
        except SubPost.DoesNotExist:
            return abort(404)
        subject_for_reporter = urllib.parse.quote(
            _("Your report on a post titled: %(title)s", title=post["title"]), safe=""
        )
        subject_for_reported_user = urllib.parse.quote(
            _("Your post titled: %(title)s", title=post["title"]), safe=""
        )
    else:
        try:
            comment = (
                SubPostComment.select()
                .where((SubPostComment.cid == report["cid"]))
                .dicts()[0]
            )
            post = ""
            logs = list(
                CommentReportLog.select()
                .join(SubPostCommentReport)
                .where(
                    (CommentReportLog.rid == report["id"])
                    | (
                        (SubPostCommentReport.cid == report["cid"])
                        & (
                            CommentReportLog.action
                            == misc.LOG_TYPE_REPORT_MODMAIL_TO_REPORTED_USER
                        )
                    )
                )
                .order_by(CommentReportLog.lid.desc())
            )
            title = SubPost.get(SubPost.pid == comment["pid"]).title
        except (SubPostComment.DoesNotExist, IndexError):
            return abort(404)
        subject_for_reporter = urllib.parse.quote(
            _("Your report on a comment on a post titled: %(title)s", title=title),
            safe="",
        )
        subject_for_reported_user = urllib.parse.quote(
            _("Your comment on a post titled: %(title)s", title=title), safe=""
        )

    reporter_modmail_url = url_for_related_modmail_thread(
        logs, misc.LOG_TYPE_REPORT_MODMAIL_TO_REPORTER
    )
    if reporter_modmail_url is None:
        reporter_modmail_url = url_for_new_modmail(
            report["reporter"], subject_for_reporter, report
        )
    reported_user_modmail_url = url_for_related_modmail_thread(
        logs, misc.LOG_TYPE_REPORT_MODMAIL_TO_REPORTED_USER
    )
    if reported_user_modmail_url is None:
        reported_user_modmail_url = url_for_new_modmail(
            reported_user.name, subject_for_reported_user, report
        )

    reported = User.select().where(User.name == report["reported"]).get()
    is_sub_banned = misc.is_sub_banned(sub, uid=reported.uid)

    return engine.get_template("mod/reportdetails.html").render(
        {
            "sub": sub,
            "report": report,
            "reported_user": reported_user,
            "related_reports": related_reports,
            "related_reports_json": json.dumps(related_report_ids, default=str),
            "banuserform": BanUserSubForm(),
            "is_sub_banned": is_sub_banned,
            "post": post,
            "comment": comment,
            "subInfo": subInfo,
            "subMods": subMods,
            "logs": logs,
            "createreportenote": CreateReportNote(),
            "reporter_modmail_url": reporter_modmail_url,
            "reported_user_modmail_url": reported_user_modmail_url,
            "url_for_modmail_thread": url_for_modmail_thread,
        }
    )


def url_for_related_modmail_thread(logs, log_type):
    """Create the url for a modmail thread, if reference to one is found
    in the report log.  url_for doesn't work, because the route is
    handled by the modmail server.

    """
    thread_log = next(filter(lambda log: log.action == log_type, logs), None)
    if thread_log is not None:
        return url_for_modmail_thread(thread_log.desc)
    return None


def url_for_modmail_thread(thread_id):
    """Given a thread id string, construct the url to view the thread in
    modmail."""
    return "/mod/mail/all/" + (
        urlsafe_b64encode(b"MessageThread " + thread_id.encode("utf-8"))
        .split(b"=")[0]
        .decode("utf-8")
    )


def url_for_new_modmail(username, subject, report):
    """Create the url to compose a new modmail thread related to a
    report."""
    return f"/mod/mail/compose?sub={report['sub']}&user={username}&subject={subject}&report_id={report['id']}&report_type={report['type']}"
