#!python3

import sys
import boto3

bucket = sys.argv[1]
filename = sys.argv[2]

client = boto3.client('s3')

client.put_object(Body=open(filename, "rb"), Bucket=bucket, Key=filename)
