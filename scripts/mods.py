#!/usr/bin/env python3
import __fix
import argparse
import sys
from peewee import fn
from app.models import User, Client, Grant, Message, SiteLog, SiteMetadata, Sub, \
    SubFlair, SubLog, SubMetadata, SubMod, SubPost, SubPostComment, \
    SubPostCommentVote, SubPostMetadata, SubPostVote, SubStylesheet, \
    SubSubscriber, Token, UserMetadata, UserSaved, \
    UserUploads, UserIgnores, \
    SubUploads, SubPostPollOption, SubPostPollVote, SubPostReport, APIToken, APITokenSettings
from app import create_app

app = create_app()

parser = argparse.ArgumentParser(description='Manage mods.')
parser.add_argument('--sub', required=True, metavar='SUB', help='Sub to modify')
parser.add_argument('--level', metavar='lvl', help='Power Level')
addremove = parser.add_mutually_exclusive_group(required=True)
addremove.add_argument('--add', metavar='USERNAME', help='Make a user a mod')
addremove.add_argument('--remove', metavar='USERNAME', help='Demod a user')
addremove.add_argument('--creator', metavar='USERNAME', help='Set the sub creator')

args = parser.parse_args()


with app.app_context():
    try:
        sub = Sub.get(fn.Lower(Sub.name) == args.sub.lower())
        subMetadata = SubMetadata(SubMetadata.sid == sub.sid)
    except Sub.DoesNotExist:
        print("No such sub")
        sys.exit(1)
    
    try:
        name = args.add or args.remove or args.creator
        user = User.get(fn.Lower(User.name) == name.lower())
    except User.DoesNotExist:
        print("Error: User does not exist")
        sys.exit(1)
    
    if args.add:
        SubMod.create(user=user.uid, sub=sub.sid, power_level=(1 if args.level == None else int(args.level)))
        print("Done.")
    elif args.remove:
        try:
            submod = SubMod.get((SubMod.user == user.uid) & (SubMod.sub == sub.sid))
            submod.delete_instance()
            print("Done.")
        except SubMod.DoesNotExist:
            print("Error: User is not a submod.")
    elif args.creator:
        try:
            subMetadata = SubMetadata.get((SubMetadata.key=='mod')&(SubMetadata.sid==sub.sid))
            subMetadata.value = user.uid
            subMetadata.save()
        except SubMetadata.DoesNotExist:
            print("SubMetadata doesn't exist?")
