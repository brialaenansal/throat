#!/bin/bash

export PGPASSWORD=${DATABASE_PASSWORD}
DATE=$(date +"%Y-%m-%d")
DIR="$(cd "$(dirname "$0")" && pwd)"
FILENAME="${DATE}_${DATABASE_NAME}.sql"

pg_dump -h $DATABASE_HOST -U $DATABASE_USER -d $DATABASE_NAME -p $DATABASE_PORT > ${FILENAME}
gzip -9 ${FILENAME}
python3 ${DIR}/upload.py ${BACKUP_BUCKET} ${FILENAME}.gz
