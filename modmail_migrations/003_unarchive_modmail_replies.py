"""Peewee migrations -- 003_unarchive_modmail_replies.py.

Removed.  The modmail reply function used to leave archived
modmails in the archive mailbox even when they got new replies
and this migration moved them to the inbox.  However, migration
042_message_threads changes the message table so the code
that used to be here no longer works, plus anyone who is
running 042 before this one is setting up a new database
and never ran the old version of modmail reply.

"""

import peewee as pw


def migrate(migrator, database, fake=False, **kwargs):
    pass

def rollback(migrator, database, fake=False, **kwargs):
    pass
