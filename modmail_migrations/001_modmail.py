"""Peewee migrations -- 001_modmail.py.

This migration originally created the modmail tables.  This work is now done
by the migrations in the main migrations directory, which should be run
before this sequence of migrations.
"""

import datetime as dt
from enum import IntEnum
import peewee as pw

def migrate(migrator, database, fake=False, **kwargs):
    pass

def rollback(migrator, database, fake=False, **kwargs):
    """Write your rollback migrations here."""
    pass
