"""Peewee migrations -- 007_order_flairs_and_rules.py.

Add ordering to the sub_post_flair and sub_rule tables.
"""

import datetime as dt
import peewee as pw


def migrate(migrator, database, fake=False, **kwargs):
    """Write your migrations here."""

    @migrator.create_model
    class SubRule(pw.Model):
        rid = pw.PrimaryKeyField()

        class Meta:
            table_name = "sub_rule"

    @migrator.create_model
    class SubFlair(pw.Model):
        xid = pw.PrimaryKeyField()

        class Meta:
            table_name = "sub_flair"

    migrator.add_fields(migrator.orm["sub_rule"], order=pw.IntegerField(null=True))
    migrator.add_fields(migrator.orm["sub_flair"], order=pw.IntegerField(null=True))

    if not fake:
        # Start them off in the order they were created.
        migrator.run()
        SubRule.update(order=SubRule.rid).execute()
        SubFlair.update(order=SubFlair.xid).execute()


def rollback(migrator, database, fake=False, **kwargs):
    """Write your rollback migrations here."""
    migrator.remove_fields("sub_rule", "order")
    migrator.remove_fields("sub_flair", "order")
