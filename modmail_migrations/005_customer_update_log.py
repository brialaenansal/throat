"""Peewee migrations -- 005_customer_update_log.py

Add a model to record changes to user status which need to be, or have
been, communicated to Stripe.

"""

import datetime as dt
import peewee as pw


class User(pw.Model):
    uid = pw.CharField(primary_key=True, max_length=40)

    class Meta:
        table_name = "user"


def migrate(migrator, database, fake=False, **kwargs):
    """Write your migrations here."""

    @migrator.create_model
    class CustomerUpdateLog(pw.Model):
        uid = pw.ForeignKeyField(db_column="uid", model=User, field="uid")
        action = pw.TextField()
        value = pw.TextField(null=True)
        created = pw.DateTimeField(default=dt.datetime.utcnow)
        completed = pw.DateTimeField(null=True)
        success = pw.BooleanField(null=True)

        class Meta:
            table_name = "customer_update_log"


def rollback(migrator, database, fake=False, **kwargs):
    """Write your rollback migrations here."""
    migrator.remove_model("customer_update_log")
